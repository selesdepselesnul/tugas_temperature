package sample;

/**
 *  Nama    : Moch Deden
 *  Kelas   : C
 *  Npm     : 41155050140062
 *  Jurusan : Informatika
 */

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.function.Function;

public class Controller implements Initializable {
    @FXML
    private Label outputLabel;

    @FXML
    private TextField tempTextField;

    @FXML
    private ComboBox<Function<Double, Double>> tempComboBox;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tempComboBox.getItems().setAll(
            new Function<Double, Double>() {
                @Override
                public Double apply(Double c) {
                    return 4/5 * c;
                }

                @Override
                public String toString() {
                    return "Celcius -> Reamur";
                }
            },
            new Function<Double, Double>() {
                @Override
                public Double apply(Double c) {
                    return 9/5 * c + 32;
                }

                @Override
                public String toString() {
                    return "Celcius -> Fahrenheit";
                }
            },
            new Function<Double, Double>() {
                @Override
                public Double apply(Double r) {
                    return 5/4 * r;
                }

                @Override
                public String toString() {
                    return "Reamur -> Celcius";
                }
            },
            new Function<Double, Double>() {
                @Override
                public Double apply(Double r) {
                    return 9/4 * r + 32;
                }

                @Override
                public String toString() {
                    return "Reamur -> Fahrenheit";
                }
            },
            new Function<Double, Double>() {
                @Override
                public Double apply(Double f) {
                    return 5/9 * f - 32;
                }

                @Override
                public String toString() {
                    return "Fahrenheit -> Celcius";
                }
            },
            new Function<Double, Double>() {
                @Override
                public Double apply(Double f) {
                    return 4/9 * f - 32;
                }

                @Override
                public String toString() {
                    return "Fahrenheit -> Reamur";
                }
            }
        );
    }

    public void onConvert(ActionEvent actionEvent) {
        String val = tempTextField.getText();
        if(tempComboBox.getSelectionModel().getSelectedItem() != null)
            if(val.matches("\\d+\\.?\\d+")) {
                double result =
                        tempComboBox
                            .getSelectionModel()
                            .getSelectedItem()
                            .apply(Double.parseDouble(val));
                outputLabel.setText("Hasilnya = " + result);
            } else {
                outputLabel.setText("Input harus angka !");
            }
        else
            outputLabel.setText("Pilih jenis konversi !");

    }

    public void onExit(ActionEvent actionEvent) {
        Platform.exit();
    }


}
